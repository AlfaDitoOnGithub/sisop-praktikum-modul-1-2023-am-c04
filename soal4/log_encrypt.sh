#! /bin/bash

#mengakses syslog haruslah root user
#konfigurasi cron
# 0 */2 * * * /soal4/log_encrypt.sh

fileLog="/var/log/syslog"

log_backup="$(date +"%H:%M %d:%m:%Y").txt"
jam=$(date +"%H")

#cara primitive 
case $jam in
	00)
	echo "$(cat $fileLog >> "$log_backup")"
	;;
	01)
	echo "$(cat $fileLog | tr [a-z] [b-za-a] | tr [A-Z] [B-ZA-A] >> "$log_backup")"
	;;
	02)
	echo "$(cat $fileLog | tr [a-z] [c-za-b] | tr [A-Z] [C-ZA-B] >> "$log_backup")"
	;;
	03)
	echo "$(cat $fileLog | tr [a-z] [d-za-c] | tr [A-Z] [D-ZA-C] >> "$log_backup")"
	;;
	04)
	echo "$(cat $fileLog | tr [a-z] [e-za-d] | tr [A-Z] [E-ZA-D] >> "$log_backup")"
	;;
	05)
	echo "$(cat $fileLog | tr [a-z] [f-za-e] | tr [A-Z] [F-ZA-E] >> "$log_backup")"
	;;
	06)
	echo "$(cat $fileLog | tr [a-z] [g-za-f] | tr [A-Z] [G-ZA-F] >> "$log_backup")"
	;;
	07)
	echo "$(cat $fileLog | tr [a-z] [h-za-g] | tr [A-Z] [H-ZA-G] >> "$log_backup")"
	;;
	08)
	echo "$(cat $fileLog | tr [a-z] [i-za-h] | tr [A-Z] [I-ZA-H] >> "$log_backup")"
	;;
	09)
	echo "$(cat $fileLog | tr [a-z] [j-za-i] | tr [A-Z] [J-ZA-I] >> "$log_backup")"
	;;
	10)
	echo "$(cat $fileLog | tr [a-z] [k-za-j] | tr [A-Z] [K-ZA-J] >> "$log_backup")"
	;;
	11)
	echo "$(cat $fileLog | tr [a-z] [l-za-k] | tr [A-Z] [L-ZA-K] >> "$log_backup")"
	;;
	12)
	echo "$(cat $fileLog | tr [a-z] [m-za-l] | tr [A-Z] [M-ZA-L] >> "$log_backup")"
	;;
	13)
	echo "$(cat $fileLog | tr [a-z] [n-za-m] | tr [A-Z] [N-ZA-M] >> "$log_backup")"
	;;
	14)
	echo "$(cat $fileLog | tr [a-z] [o-za-n] | tr [A-Z] [O-ZA-N] >> "$log_backup")"
	;;
	15)
	echo "$(cat $fileLog | tr [a-z] [p-za-o] | tr [A-Z] [P-ZA-O] >> "$log_backup")"
	;;
	16)
	echo "$(cat $fileLog | tr [a-z] [q-za-p] | tr [A-Z] [Q-ZA-P] >> "$log_backup")"
	;;
	17)
	echo "$(cat $fileLog | tr [a-z] [r-za-q] | tr [A-Z] [R-ZA-Q] >> "$log_backup")"
	;;
	18)
	echo "$(cat $fileLog | tr [a-z] [s-za-r] | tr [A-Z] [S-ZA-R] >> "$log_backup")"
	;;
	19)
	echo "$(cat $fileLog | tr [a-z] [t-za-s] | tr [A-Z] [T-ZA-S] >> "$log_backup")"
	;;
	20)
	echo "$(cat $fileLog | tr [a-z] [u-za-t] | tr [A-Z] [U-ZA-T] >> "$log_backup")"
	;;
	21)
	echo "$(cat $fileLog | tr [a-z] [v-za-u] | tr [A-Z] [V-ZA-U] >> "$log_backup")"
	;;
	22)
	echo "$(cat $fileLog | tr [a-z] [w-za-v] | tr [A-Z] [W-ZA-V] >> "$log_backup")"
	;;
	23)
	echo "$(cat $fileLog | tr [a-z] [x-za-w] | tr [A-Z] [X-ZA-W] >> "$log_backup")"
	;;
esac

