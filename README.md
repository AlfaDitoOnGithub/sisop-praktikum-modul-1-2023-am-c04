# sisop-praktikum-modul-1-2023-AM-C04
Terdiri dari:
- Alfadito Aulia Denova | 5025211157 
- Layyinatul Fuadah | 5025211207 
- Armadya Hermawan S | 5025211243 



## Penjelasan Soal dan Jawaban
## Soal 1 - University Survey
Soal meminta user untuk melakukan serangkaian operasi pada file .csv, yang nantinya akan mengeluarkan baris dengan kriteria tertentu. 

Ada 4 buah pencarian yang dilakukan, pertama adalah mencari top 5 di jepang.
     ` cat '2023 QS World University Rankings.csv' | grep -i "Japan" | sort -t "," -k1 -n| head -n 5| awk -F "," '{print $1, $2, $4}'`
Script diatas akan membaca file 2023 QS WOrld University Rankings.csv, kemudian memcari semua baris yang berlokasi di jepang, lalu di sort sesuai k1 lalu dari paling atas mengambil 5 baris.

kedua, dari hasil sebelumnya carilah yang fsr scorenya rendah
     `cat '2023 QS World University Rankings.csv' | grep -i "Japan" | sort -t "," -k1 -rn| head -n 5| sort -t "," -k9 -n| awk -F "," '{print $1, $2, $9}'`
mirip dengan sebelumnya, tetapi ditambah dengan sort sesuai dengan kategori k9 sehingga mendapatkan fsr score yang paling rendah diambil 5 baris pertamanya saja. 
///////

-catatan revisi: hasil yang diberikan keliru, tetapi kami tidak yakin salahnya dimana, karena lupa menanyakan jawaban yang benarnya

ketiga, cari 10 universitas di jepang yang ger rank nya dari tertinggi ke terendah
     `cat '2023 QS World University Rankings.csv' |grep -i "japan" | sort -t "," -k20 -n| head -n 10| awk -F "," '{print $1, $2, $20}'`
tidak begitu berbeda, hanya filternya kita cari pada kolom ke 20 dan ambil 10 barisnya

Terakhir, mencari universitas dengan keyword 'Keren' di dalamnya
     `cat '2023 QS World University Rankings.csv' | grep -i "Keren" | awk -F "," '{print $1, $2}'`
tinggal melakukan cat dan di filter menggunakan grep.

## Soal 2 - Kobeni Liburan
Script kobeni_.sh merupakan sebuah script bash yang berfungsi untuk mendownload sebuah file gambar dari internet dan menyimpannya ke dalam folder yang di-generate secara otomatis. Script ini juga akan meng-zip folder-folder tersebut berdasarkan jumlah folder yang telah di-generate.

```sh
hour=$(date "+%H")
countx=$(($hour == "00" ? 1 : $hour))
```
``hour=$(date "+%H")``ini digunakan 
untuk mengambil waktu saat ini dalam format 24 jam

``$countx=$(($hour == "00" ? 1 : $hour))`` ini digunakan untuk menentukan jumlah kali download. Jika waktu saat ini adalah 00:00 maka file yang akan didownload berjumalh 1, jika tidak makan file yang akan didownload sebnyak jumlah jam dalam waktu saat ini

```sh
folder_count=$(find . -maxdepth 1 -type d -name "kumpulan_*" | wc -l)

if [ "$folder_count" -eq 0 ]; then
  nama_folder="kumpulan_1.FOLDER"
else
  next_number=$((folder_count + 1))
  nama_folder="kumpulan_${next_number}.FOLDER"
fi
mkdir -p "$nama_folder"
```
``folder_count=$(find . -maxdepth 1 -type d -name "kumpulan_*" | wc -l)``

ini digunakan untuk menghitung jumlah direktori yang ditemukan di direktori saat ini yang memiliki nama awalan "kumpulan_", dan menyimpan hasilnya dalam variabel 'folder_count'.

``if [ "$folder_count" -eq 0 ]; then
  nama_folder="kumpulan_1.FOLDER"
else
  next_number=$((folder_count + 1))
  nama_folder="kumpulan_${next_number}.FOLDER"
fi``

digunakan untuk menamakan foldernya, jika foldernya masih 0 maka namanya akan menjadi kumpulan_1.FOLDER, jika foldernya ada 1 makan namanya akan menjadi kumpulan_2.FOLDER dan seterusnya.

``mkdir -p "$nama_folder"
`` digunakan untuk akan membuat sebuah direktori dengan nama yang ditentukan dalam variabel $nama_folder dan juga membuat direktori parent jika belum ada.

```sh
# Loop to download file specified number of times
for ((index=1; index<=$countx; index++)); do
  # Set filename to download
  nama_file="perjalanan_${index}.FILE"

  # Download file
  wget -O "${nama_folder}/${nama_file}" https://www.nationsonline.org/gallery/Indonesia/Piaynemo-West-Papua.jpg

  # Move file to folder
  mv "${nama_folder}/${nama_file}" "$nama_folder"

  echo "File berhasil didownload ke folder $nama_folder"
  
done
```
``for ((index=1; index<=$countx; index++)); do`` digunakan untuk melakukan loopingan countx yang sudah ditetapkan

``nama_file="perjalanan_${index}.FILE"`` digunakan untuk menamakan file yang akan di download

``wget -O "${nama_folder}/${nama_file}" https://www.nationsonline.org/gallery/Indonesia/Piaynemo-West-Papua.jpg
`` digunakan untuk mendownload file yang berada di source yang sudah ditulis, lalu gunakan -0 untuk bisa menamakan file tersebut

`` mv "${nama_folder}/${nama_file}" "$nama_folder"`` digunakan untuk memindahkan file yang baru saja didownload ke dalam folder yang kita inginkan

```sh
zip_name="devil_$(ls -d devil_* 2>/dev/null | wc -l | awk '{print $1+1}')"
folder_count=$(ls -d kumpulan_* 2>/dev/null | wc -l | awk '{print $1-1}')
```
``zip_name="devil_$(ls -d devil_* 2>/dev/null | wc -l | awk '{print $1+1}')"`` digunakan untuk membuat nama file zip dengan menghitung jumlah file atau direktori yang sudah ada dengan awalan "devil_", kemudian menambahkan 1 ke nomor urutan file zip tersebut dan menyimpan nama file zip yang akan dibuat dalam variabel zip_name.

``folder_count=$(ls -d kumpulan_* 2>/dev/null | wc -l | awk '{print $1-1}')``digunakan untuk menghitung jumlah direktori yang sudah ada dengan awalan "kumpulan_", kemudian mengurangi 1 untuk menghilangkan direktori saat ini dari hitungan dan menyimpan hasilnya dalam variabel folder_count.
```sh
if [ $(expr $folder_count % 5) -eq 3 ]
then
zip -r $zip_name kumpulan_$((folder_count-2)).FOLDER kumpulan_$((folder_count-1)).FOLDER kumpulan_$folder_count.FOLDER
elif [ $(expr $folder_count % 5) -eq 0 ]
then
zip -r $zip_name kumpulan_$((folder_count-1)).FOLDER kumpulan_$folder_count.FOLDER
fi
```
``if [ $(expr $folder_count % 5) -eq 3 ]
then
zip -r $zip_name kumpulan_$((folder_count-2)).FOLDER kumpulan_$((folder_count-1)).FOLDER kumpulan_$folder_count.FOLDER`` digunakan untuk meng-zipkan folder yang sudah tentukan, disini kita gunakan modulus 5 jika hasilnya 3 makan kita akan mengzip 3 folder

``elif [ $(expr $folder_count % 5) -eq 0 ]
then
zip -r $zip_name kumpulan_$((folder_count-1)).FOLDER kumpulan_$folder_count.FOLDER`` digunakan untuk melakukan zip sebanyak 2 folder

lalu sesuai dengan ketentuan soal script yang membuat kita harus menjalankan setiap 10 jam maka kita akan menggunakan cronjob dengan syntax cronjob

```sh
0 */10 * * * $(pwd)/kobeni_.sh
```


## Soal 3 - Peter Grifin
pada soal kita disuruh untuk membuat sebuah sistem register dan sistem login dari setiap user yang berhasil di daftarkan di dalam file /users/users.txt dan dimana setiap percobaan melakukan register atau login maka akan tercatat pada file log.txt maka yang pertama membuat file users dan log.txt terlebih dahulu lalu membuat file louish.sh untuk membuat sistem register
    # louish.sh
    '''bash   
    echo "REGISTER"
    echo "Masukkan Username: "
    read nama
    if grep -q "^$nama:" ./users/users.txt; then
    echo "Username already exists."
    echo "REGISTER: ERROR User already exists" >> log.txt
    exit 1
    fi
'''
- skrip bash itu untuk meminta pengguna memasukkan nama pengguna/username
- Pada skrip  if grep -q "^$nama:" ./users/users.txt; then digunakan untuk memeriksa apakah username yang diinputkan oleh pengguna telah terdaftar sebelumnya dalam file users.txt pada direktori ./users/.
- jika nama pengguna telah ditemukan dalam file users.txt, maka kondisi tersebut akan dieksekusi.

- catatan revisi : tidak terdapat tanggal bulan tahun dan jam pada log.txt
harusnya :
    '''
    echo "$(date +'%y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> log.txt
    exit 1
    fi
'''
    '''
    while true; do
    echo "Password:"
    read password
    if [[ ${#password} -ge 8 && "$password" =~ [0-9] && "$password" =~ [a-z] && "$password" =~ [A-Z] && "$password" != "$name" && "$password" != "chicken" && "$password" != "ernie" ]]; then
    break
    else
    echo "repeat"
    fi
    done
'''
- meminta pengguna untuk memasukkan kata sandi dan membaca input
- lalu memeriksa apakah kata sandi memenuhi persyaratan tertentu menggunakan serangkaian kondisi dalam pernyataan if.
- Syaratnya, kata sandi harus memiliki panjang minimal 8 karakter, berisi minimal satu huruf kecil minimal satu huruf besar, dan minimal satu digit. Ini juga memeriksa bahwa kata sandi tidak sama dengan nama pengguna dan bukan salah satu dari kata sandi "ayam" dan "ernie" yang umum digunakan.
- Jika kata sandi memenuhi semua persyaratan, loop akan terputus dan skrip berlanjut. Jika tidak, ia mencetak "ulangi" untuk meminta pengguna memasukkan kata sandi yang valid lagi.

    '''
    echo "$nama:$password" >> ./users/users.txt
    echo "User registered successfully"
    echo "REGISTER: INFO User $nama registered successfully" >> log.txt
'''
- untuk menambahkan informasi pengguna baru ke file users.txt dan mencatat pesan sukses ke file log.txt.
- catatan revisi : tidak terdapat tanggal bulan tahun dan jam pada log.txt
harusnya :
    '''
    echo "$(date +'%y/%m/%d %H:%M:%S') REGISTER: INFO User $nama registered successfully" >> log.txt
'''
karena saya belum membuat sistem login maka saya akan membuat revisi untuk membuat sistem login dengan menggunakan file retep.sh , seperti perintah pada soal
    #retep.sh
    '''
    echo "LOGIN"
    echo "Masukkan Username: "
    read nama
    if grep -q "^$nama:" ./users/users.txt; then
    password=$(grep "^$nama:" ./users/users.txt | cut -d ":" -f 2)
    else
    echo "login gagal"
    echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $nama" >> log.txt
    exit 1
    fi
'''
- skrip bash itu untuk meminta pengguna memasukkan nama pengguna/username untuk login
- memeriksa apakah username ada dalam file bernama "users.txt". 
- Jika nama pengguna ada, ia mengekstrak kata sandi untuk pengguna tersebut dari file, jika tidak, ia mengeluarkan pesan kesalahan dan mencatat upaya login yang gagal ke file bernama "log.txt".

    '''
    echo "Password:"
    read pass
    if [[ "$pass" == "$password" ]]; then
    echo "Login berhasil"
    echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: INFO User $nama logged in" >> log.txt
    else
    echo "login gagal"
    echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $nama" >> log.txt
    exit 1
    fi
'''
- memasukkan password pengguna,jika kata sandi cocok dengan yang disimpan dalam file, itu menghasilkan pesan sukses dan mencatat upaya masuk ke file bernama "log.txt". 
- Jika tidak, itu menampilkan pesan kesalahan dan mencatat upaya login yang gagal ke file yang sama.



## Soal 4 - Encrypt dan Decrypt file log
soal meminta untuk melakukan backup log dan mengenkripsi isinya, juga membuat dekripsi dari file enkripsi tersebut
hal ini mudah dilakukan dengan menggunakan fungsi 'tr' dimana seluruh huruf alfabet akan berubah menjadi sesuai dengan pengaturan yang diberikan

contoh:`tr [a-z] [b-za-a]` akan mengubah a menjadi b, atau bertambah satu. 

tinggal kita sesuaikan berapa perpindahannya sesuai dengan jam yang file dibuat, cara yang kami gunakan adalah cara primitif dengan switch-case pada linux.

-catatan revisi: fungsi decrypt hanya bekerja pada file dengan jam paling besar, i.e. yang di encrypt terakhir pada hari itu, jika ada enkripsi pada hari berikutnya, tapi tidak menghapus yang hari sebelumnya, maka decrypt akan mengerjakan yang jamnya paling besar. hal ini dikarenakan fungsi pengambilan filenya secara default mengambil dari list yang paling akhir

